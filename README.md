# FizzBuzz

[![CircleCI](https://circleci.com/bb/luismachado5/fizzbuzz.svg?style=svg)](https://circleci.com/bb/luismachado5/fizzbuzz)
[![codecov](https://codecov.io/bb/luismachado5/fizzbuzz/branch/master/graph/badge.svg)](https://codecov.io/bb/luismachado5/fizzbuzz)
[![codebeat badge](https://codebeat.co/badges/404ca66c-ffda-40bf-ba78-4202a33ef946)](https://codebeat.co/projects/bitbucket-org-luismachado5-fizzbuzz-master)
[![Swift 4.0](https://img.shields.io/badge/swift-4.0-green.svg?style=flat)](https://developer.apple.com/swift)
[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)

Base app to apply and test methodologies for test driven development (TDD) and Continuous Integration (CI).

For TTD it will feature:
- Quick
- Nimble
- Nimble + Snapshot

For CI it will feature:
- Circle CI
- CodeCov
- SwiftLint
- Codebeat
