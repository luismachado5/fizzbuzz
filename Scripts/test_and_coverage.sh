#!/bin/bash

xcodebuild clean test -workspace FizzBuzz.xcworkspace -scheme FizzBuzz -sdk iphonesimulator -destination "platform=iOS Simulator,OS=11.2,name=iPhone 8" CODE_SIGN_IDENTITY="" CODE_SIGNING_REQUIRED=NO ONLY_ACTIVE_ARCH=NO -enableCodeCoverage YES | xcpretty -c